export const toggleModal = (
  pathname,
  history
) => {
  history.push(pathname);
};
